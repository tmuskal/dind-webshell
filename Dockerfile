FROM jpetazzo/dind

RUN apt-get update && apt-get -y install shellinabox sudo procps wget

# a new user is needed to get access to the container

RUN echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    useradd -u 5001 -G users,sudo -d /home/user --shell /bin/bash -m user && \
    echo "secret\nsecret" | passwd user

USER user

EXPOSE 4200

CMD ["sudo","shellinaboxd","--css","/etc/shellinabox/options-available/00_White On Black.css", "-s", "/:user:users:/home/user:/bin/bash", "--disable-ssl"]